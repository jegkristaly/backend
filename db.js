const config = require('./config/config');
const mongoose = require('mongoose');

'use strict';

mongoose.Promise = global.Promise;


const promise = mongoose.connect(`mongodb://${config.mongo.dbuser}:${config.mongo.dbpass}@${config.mongo.dburl}`,
    {useMongoClient: true});

promise.then(
    () => {console.log("Successfully connected to DB server");},
    err => {console.log(`Connecting to the db resulted in error : ${err}`);}
);

module.exports = mongoose;
