#  Sensor backend

Sensor backend using Mongo, Express, Node.

To use locally:
```bash
npm install
```

* Powershell
    * `$env:DEBUG='backend:*'`
    * `npm run local`
* Bash:
    * `DEBUG=backend:* npm run local`

[API documentation](https://sensor-backend.herokuapp.com/api-docs/)