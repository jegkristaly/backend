Array.prototype.diff = function(other) {
    return this.filter(function(i) {return other.indexOf(i) < 0;});
};
