const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const measurementSchema = new Schema({
    live: {type: Boolean, default: true},
    name: {type: String, required: true},
    startDate: {type: Date, default: Date.now},
    duration: Number,
    creator: {type: String, required: true},
    sensors: [{type: Schema.Types.ObjectId, ref: 'Sensors'}],
    finished: {type: Boolean, default: false},
    __v: {type: Number, select: false}
});


const Measurements = mongoose.model('Measurements', measurementSchema);

module.exports = Measurements;