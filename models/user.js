const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

'use strict';

const userTypes = {
    values: ['normal', 'admin'],
    message: 'user type must be in enum userTypes',
    normal: 'normal',
    admin: 'admin'
};

const userSchema = new Schema({
    username: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    userType: {type: String, enum: userTypes, default: userTypes.normal}
});

userSchema.pre('validate', function (next) {
    let user = this;
    if (user.isNew && user.password.length > 72) {
        const err = new Error("Password too long.");
        next(err);
    }
    else {
        next();
    }
});

userSchema.pre('save', function (next) {
    let user = this;
    if (user.isNew || user.isModified('password')) {
        bcrypt.genSalt(9, (err, salt) => {
            if (err)
                return next(err);

            bcrypt.hash(user.password, salt, (err, hash) => {
                if (err)
                    return next(err);

                user.password = hash;
                next();
            });
        });
    }
    else
        return next();
});

userSchema.methods.comparePassword = function (password, callback) {
    let user = this;
    bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};


const Users = mongoose.model('Users', userSchema);

module.exports = Users;
module.exports.userTypes = userTypes;