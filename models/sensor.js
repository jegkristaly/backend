const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const distributionTypes = {
    values: ['random', 'linear', 'logarithmic'],
    message: 'distribution must be in enum distributionTypes'
};

const dataSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, ref: 'Measurements'},
    data: []
});

const sensorSchema = new Schema({
    name: {type: String, required: true},
    type: {type: String, required: true},
    unit: String,
    distribution: {type: String, enum: distributionTypes, required: true, lowercase: true},
    min: {type: Number, default: 0},
    max: {type: Number, default: 4000},
    interval: {type: Number, default: 2},
    data: [dataSchema],
    coordinates: {lat: Number, long: Number},
    __v: {type: Number, select: false},
});


const Sensors = mongoose.model('Sensors', sensorSchema);
module.exports = Sensors;
module.exports.distributionTypes = distributionTypes.values;
