const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongo = require('./db'); //initialize
const passport = require('./config/passport');
const morgan = require('morgan');

const measurements = require('./routes/measurements');
const sensors = require('./routes/sensors');
const users = require('./routes/users');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./openapi.json');

const app = express();

'use strict';

app.enable('trust proxy');

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function (err, req, res, next) {
    if (err instanceof SyntaxError) {
        return res.status(400).json({message: "Bad request"});
    }
    else {
        next();
    }
});

if (app.settings.env === 'production') {
    app.use(morgan('common', {skip: (req, res) => res.statusCode < 400})
    );
} else {
    app.use(morgan('dev'));
}

app.use(passport.initialize());


app.use('/api/measurements', measurements);
app.use('/api/sensors', sensors);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/', users);

module.exports = app;
