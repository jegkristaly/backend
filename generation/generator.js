const Scheduler = require('./scheduler');
const genMethods = require('./gen-methods');
const Sensor = require('../models/sensor');
const Measurement = require('../models/measurement');

class Generator {

    constructor(measurement, sensors) {

        this.measurementId = measurement._id.toString();
        this.sensors = sensors || measurement.sensors;
        this.startDate = measurement.startDate;
        this.endDate = new Date(this.startDate.getTime() + measurement.duration * 1000);

        if (!measurement.live)
            this.scheduleLive(this.startDate, true);
        this.scheduleLive(this.endDate, false);
    }


    async scheduleLive(date, status) {
        Scheduler.scheduleOnce(date, () => {
            Measurement.findById(this.measurementId, (err, measurement) => {
                if (err || measurement === null) {
                    console.log(err);
                    return;
                }
                measurement['live'] = status;
                measurement['finished'] = !status;
                measurement.save((err) => {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        });
    }

    async generateJobs() {
        for (let i = 0, curr; i < this.sensors.length; i++) {
            curr = this.sensors[i];
            try {
                let sensor = await Sensor.findById(curr);
                if (sensor !== null) {
                    new Scheduler(
                        this.startDate,
                        this.endDate,
                        sensor["interval"],
                        genMethods.methods[sensor.distribution],
                        (value) => {
                            let subdoc = sensor.data.id(this.measurementId);
                            if (subdoc === null) {
                                sensor.data.push({_id: this.measurementId});
                                subdoc = sensor.data.id(this.measurementId);
                            }
                            subdoc.data.push({value: value, timeStamp: Date.now()});
                            sensor.markModified('data');
                            return sensor.save();
                        },
                        {min: sensor.min, max: sensor.max}
                    ).schedule();

                }
            } catch (error) {
                console.log(error);
            }
        }
    }
}

module.exports = Generator;