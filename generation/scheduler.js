const scheduler = require('node-schedule');

class Scheduler {
    constructor(startDate, endDate, interval, genMethod, storeMethod, genOptions) {
        this.start = startDate;
        this.end = endDate;
        this.interval = interval;
        this.getNext = genMethod;

        let defOptions = {
            x: .1,
            startValue: 0,
            increment: 1,
            base: null,
            min: null,
            max: null
        };


        this.options = Object.assign(defOptions, genOptions);
        this.curr = this.options.startValue;
        this.storeMethod = storeMethod
    }

    static scheduleOnce(start, todo) {
        scheduler.scheduleJob(start, () => todo());
    }

    async _generate() {
        this.curr = await this.getNext(this.curr, this.options); //might take some time if we actually polled a sensor

        //store
        try {
            await this.storeMethod(this.curr);
        }
        catch (err) {
            console.log(err);
        }
    }

    schedule() {
        this.job = scheduler.scheduleJob({
                start: this.start,
                end: this.end,
                rule: `*/${this.interval} * * * * *`
            },
            () => this._generate());
    }
}


module.exports = Scheduler;