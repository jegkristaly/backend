function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

function getLogarithm(x, base) {
    if (base === null || base === 1 || base <= 0)
        return Math.log2(x);
    return Math.log2(x) / Math.log2(base);
}


function linearNext(curr, options) {
    const next = curr + (options.increment || 1);
    if (options.increment > 0 && next < options.max)
        return next;
    else if (options.increment > 0)
        return options.max;
    else if (options.increment < 0 && next > options.min)
        return next;
    else
        return options.min;
}

function randomNext(curr, options) {
    return getRandom(options.min, options.max);
}


function logarithmicNext(curr, options) {
    let res = getLogarithm(options.x, options.base);
    options.x += options.increment || 1;
    return res;
}

exports.linear = linearNext;
exports.random = randomNext;
exports.logarithmic = logarithmicNext;

const methods = {
    "linear": linearNext,
    "random": randomNext,
    "logarithmic": logarithmicNext
};

exports.methods = methods;