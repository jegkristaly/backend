const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('./config');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');

'use strict';

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.secret,
    issuer: config.issuer,
    ignoreExpiration: false,
    algorithms: ['HS256']
};

passport.use(new JwtStrategy(opts, function (payload, done) {
    User.findById(payload.id, (err, user) => {
        if (err) {
            return done(err, false);
        }
        else if (!user) {
            return done(null, false, {message: 'Incorrect username.'});
        }
        else {
            return done(null, user);
        }
    })
}));

module.exports = passport;