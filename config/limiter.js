const RateLimit = require('express-rate-limit');

'use strict';

const limiter = new RateLimit({
    windowMs: 60 * 60 * 1000, //in 60 mins
    max: 20, // max 20 requests
    delayMs: 0
});

module.exports = limiter;