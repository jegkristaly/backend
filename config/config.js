const config = {
    "mongo" :{
        "dbuser":process.env.DBUSER || "@youruser",
        "dbpass":process.env.DBPASS || "@yourpassword",
        "dburl":process.env.DBURL || "@yourserver",
        "dburldev":process.env.DBURLDEV || "@yourdevserver"
    },

    "secret": process.env.SECRET || "@yoursecret",
    "issuer": process.env.ISSUER || "@yourissuer"
};

module.exports = config;