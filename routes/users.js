const express = require('express');
const router = express.Router();
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/config');
const limiter = require('../config/limiter');

'use strict';
router.use(limiter);

router.post('/register', function (req, res) {
    let user = new User(req.body);
    if (!user.username || !user.password) {
        return res.status(400).json({message: "Missing username or password."});
    }

    user.save((err) => {
        if (err) {
            // console.log(err);
            return res.status(400).json({message: "Username already taken or fields are invalid."});
        }
        return res.status(202).json({message: "New user added."});
    });
});

router.post('/login', (req, res) => {
    let usr = {username: req.body.username, password: req.body.password};
    if (!usr.username || !usr.password) {
        return res.status(400).json({message: "Missing username or password."});
    }

    User.findOne({username: usr.username}, (err, user) => {
        if (err)
            return res.status(500).json({message: "Server error."});

        if (!user) {
            res.status(401).json({message: "User not found."});
        }
        else {
            user.comparePassword(usr.password, (err, isMatch) => {
                if (!err && isMatch) {
                    const token = jwt.sign(
                        {id: user.id},
                        config.secret,
                        {expiresIn: "6h", issuer: config.issuer}); //expires in 6 hrs

                    return res.json({token: token, expiresIn: '6h'});
                }
                else {
                    res.status(401).json({message: "Authentication failed."});
                }
            })
        }
    })
});


module.exports = router;