const express = require('express');
const router = express.Router();
const generator = require('../generation/generator');
const Measurement = require('../models/measurement');
const passport = require('passport');
const userTypes = require('../models/user').userTypes;

require('../util');

'use strict';
router.use(passport.authenticate('jwt', {session: false}));

router.get('/', function (req, res) {
    Measurement.aggregate({$project: {name: true, live: true}})
        .then((docs) => res.json(docs));
});

router.post('/', function (req, res) {
    delete req.body['finished'];
    delete req.body['live'];

    let username = req.user.username;

    let measurement = new Measurement(req.body);
    if (measurement.startDate > new Date(Date.now()))
        measurement.live = false;

    measurement.creator = username;


    measurement.save((err, product) => {
        if (err) {
            // console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        // noinspection JSIgnoredPromiseFromCall
        new generator(product.toObject()).generateJobs();

        res.json({_id: product._id});
    });
});

router.get('/:id', function (req, res) {
    const id = req.params.id;
    Measurement.findById(id, (err, measurement) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (measurement === null) {
            return res.status(204).send();
        }
            res.json(measurement);
        }
    );
});

router.put('/:id', function (req, res) {
    const id = req.params.id;
    const user = req.user;
    Measurement.findById(id, (err, measurement) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (measurement === null || measurement['finished']) {
            return res.status(204).send();
        }

        if (user.username !== measurement.creator && user.userType !== userTypes.admin)
            return res.status(403).json({message: "Unauthorized."});

        let isSensorChanged = false;
        let sensorsAdded = [];
        for (let prop in req.body) {
            switch (prop) { //don't modify fields that might cause anomalies
                case "_id":
                case "id":
                case "duration":
                case "live":
                case "startDate":
                case "finished":
                case "creator":
                    break;

                case "sensors":
                    sensorsAdded = req.body[prop].diff(measurement[prop]);

                    //remove some if necessary
                    measurement[prop] = req.body[prop];
                    measurement.markModified(prop);

                    if (sensorsAdded !== null && sensorsAdded.length !== 0)
                        isSensorChanged = true;
                    break;

                default:
                    if (measurement.get(prop)) { //check if prop exists in schema
                        measurement[prop] = req.body[prop];
                        measurement.markModified(prop);
                    }
                    break;
            }
        }

        measurement.save((err, product, numAffected) => {
            if (err) {
                // console.log(err);
                return res.status(400).json('Bad Request');
            }

            if (sensorsAdded) { // noinspection JSIgnoredPromiseFromCall
                new generator(product.toObject(), sensorsAdded).generateJobs();
            }

            res.json({numAffected});
        });
    });
});

router.delete('/:id', function (req, res) {
    const id = req.params.id;
    const user = req.user;

    Measurement.findById(id, (err, measurement) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (measurement === null) {
            return res.status(204).send();
        }

        if (user.username !== measurement.creator && user.userType !== userTypes.admin)
            return res.status(403).json({message: "Unauthorized."});

        measurement.remove((err, product) => {
            if (err) {
                //console.log(err);
                return res.status(400).json({message: 'Bad Request.'});
            }
            if (product === null) {
                return res.status(204).send();
            }

            return res.json({numAffected: 1});
        });
    });
});

module.exports = router;
