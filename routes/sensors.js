const express = require('express');
const router = express.Router();
const Sensor = require('../models/sensor');
const passport = require('passport');
const userTypes = require('../models/user').userTypes;

'use strict';
router.use(passport.authenticate('jwt', {session: false}));

function adminOnly(req, res, next) {
    let user = req.user;
    if (user.userType !== userTypes.admin)
        return res.status(403).json({message: "Unauthorized."});
    else
        next();
}

router.get('/', function (req, res) {
    Sensor.aggregate({$project: {name: true, type: true}}).then((sensor) => res.json(sensor));
});

router.get('/:id', function (req, res) {
    const id = req.params.id;
    Sensor.findById(id, {data: false}, (err, sensor) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (sensor === null)
            return res.status(204).send();

        res.json(sensor);
    });
});

router.get('/:id/data/:measurement', function (req, res) {
    const id = req.params.id;
    const measurementId = req.params.measurement;
    Sensor.findById(id, (err, sensor) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (sensor === null)
            return res.status(204).send();

        let subdoc = sensor.data.id(measurementId);
        if (subdoc === null)
            res.status(204).send();
        else
            res.json(subdoc);
    });
});

router.get('/:id/data/:measurement/latest', function (req, res) {
    const id = req.params.id;
    const measurementId = req.params.measurement;
    Sensor.findById(id, (err, sensor) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (sensor === null)
            return res.status(204).send();

        const subdoc = sensor.data.id(measurementId).data;
        if (subdoc === null)
            res.status(204).send();
        else
            res.json(subdoc[subdoc.length - 1]);
    });
});

router.post('/', adminOnly, function (req, res) {
    let sensor = new Sensor(req.body);
    sensor.save((err, product) => {
        if (err) {
            // console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        res.json({_id: product._id});
    });
});

router.put('/:id', adminOnly, function (req, res) {
    if (user.userType !== user.userTypes.admin)
        return res.status(401).json({message: "Unauthorized."});

    const id = req.params.id;
    Sensor.findById(id, (err, sensor) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (sensor === null)
            return res.status(204).send();

        for (let prop in req.body) {
            if (prop === "_id" || prop === "id" || prop === "data") {
            } //dont modify id's, since that might cause anomalies
            else if (sensor.get(prop)) { //check if prop exists in schema
                sensor[prop] = req.body[prop];
                sensor.markModified(prop);
            }
        }

        sensor.save((err, product, numAffected) => {
            if (err) {
                // console.log(err);
                return res.status(400).json({message: 'Bad Request.'});
            }
            res.json({numAffected});
        });
    });
});

router.delete('/:id', adminOnly, function (req, res) {
    if (user.userType !== user.userTypes.admin)
        return res.status(401).json({message: "Unauthorized."});

    const id = req.params.id;
    console.log("delete");
    Sensor.findByIdAndRemove(id, (err, sensor) => {

        if (err) {
            //console.log(err);
            return res.status(400).json({message: 'Bad Request.'});
        }
        if (sensor === null)
            return res.status(204).send();

        res.json({numAffected: 1}); // since id's are unique, and nothing has failed
    });
});

module.exports = router;